package cn.wm.service;

import cn.wm.model.ClassType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-13
 */
public interface ClasstypeService extends IService<ClassType> {

}
