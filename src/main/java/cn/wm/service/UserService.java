package cn.wm.service;

import cn.wm.model.User;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */

public interface UserService extends IService<User> {


    IPage findAllPage(Integer pageNo, Integer pageSize, String courseName);

    List<User> findAll();

    List<User> findByName(String name);

    int updateUser(User user);
}
