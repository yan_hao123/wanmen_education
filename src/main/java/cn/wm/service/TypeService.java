package cn.wm.service;

import cn.wm.model.Type;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
public interface TypeService extends IService<Type> {

    Map findSort();
}
