package cn.wm.service;

import cn.wm.model.Course;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-13
 */
public interface CourseService extends IService<Course> {

    IPage findAllPage(Integer pageNo, Integer pageSize,String courseName);

    List<Course> findAll();
}
