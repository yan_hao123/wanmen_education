package cn.wm.service;

import cn.wm.model.TbSysPermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
public interface TbSysPermissionService extends IService<TbSysPermission> {

}
