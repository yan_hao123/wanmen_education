package cn.wm.service.impl;

import cn.wm.model.ClassType;
import cn.wm.mapper.ClasstypeMapper;
import cn.wm.service.ClasstypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-13
 */
@Service
public class ClasstypeServiceImpl extends ServiceImpl<ClasstypeMapper, ClassType> implements ClasstypeService {


}
