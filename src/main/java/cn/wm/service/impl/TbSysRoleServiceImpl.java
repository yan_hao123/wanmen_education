package cn.wm.service.impl;

import cn.wm.model.TbSysRole;
import cn.wm.mapper.TbSysRoleMapper;
import cn.wm.service.TbSysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@Service
public class TbSysRoleServiceImpl extends ServiceImpl<TbSysRoleMapper, TbSysRole> implements TbSysRoleService {

}
