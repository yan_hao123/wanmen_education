package cn.wm.service.impl;

import cn.wm.model.Cycle;
import cn.wm.mapper.CycleMapper;
import cn.wm.service.CycleService;
import cn.wm.utils.QiniuCloudUtil;
import cn.wm.utils.SYS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@Service
public class CycleServiceImpl extends ServiceImpl<CycleMapper, Cycle> implements CycleService {

    @Autowired
    private CycleMapper cycleMapper;

    @Override
    @Transactional
    public boolean deleteById(Integer pid) {
            Cycle cycle = cycleMapper.selectById(pid);
            String pUrl = cycle.getPUrl();
            QiniuCloudUtil.delImg(pUrl.replace(SYS.URL,""));
            cycleMapper.deleteById(pid);
            return true;
    }
}
