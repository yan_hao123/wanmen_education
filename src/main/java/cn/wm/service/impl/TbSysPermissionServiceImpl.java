package cn.wm.service.impl;

import cn.wm.model.TbSysPermission;
import cn.wm.mapper.TbSysPermissionMapper;
import cn.wm.service.TbSysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@Service
public class TbSysPermissionServiceImpl extends ServiceImpl<TbSysPermissionMapper, TbSysPermission> implements TbSysPermissionService {

}
