package cn.wm.service.impl;

import cn.wm.model.Classes;
import cn.wm.mapper.ClassesMapper;
import cn.wm.service.ClassesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@Service
public class ClassesServiceImpl extends ServiceImpl<ClassesMapper, Classes> implements ClassesService {

}
