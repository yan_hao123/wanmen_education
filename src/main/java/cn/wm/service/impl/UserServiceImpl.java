package cn.wm.service.impl;

import cn.wm.model.User;
import cn.wm.mapper.UserMapper;
import cn.wm.service.UserService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;


    @Override
    public IPage findAllPage(Integer pageNo,Integer pageSize,String userName) {
        Page page = new Page(pageNo,pageSize);
        IPage iPage = userMapper.findAllPage(page,userName);
        return iPage;
    }

    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }

    @Override
    public List<User> findByName(String name) {
        return userMapper.findByName(name);
    }

    @Override
    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }

}
