package cn.wm.service.impl;

import cn.wm.model.Orders;
import cn.wm.mapper.OrdersMapper;
import cn.wm.service.OrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {

}
