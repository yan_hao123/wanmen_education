package cn.wm.service.impl;

import cn.wm.mapper.UserMapper;
import cn.wm.model.Teacher;
import cn.wm.mapper.TeacherMapper;
import cn.wm.service.TeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements TeacherService {

    @Autowired
    private TeacherMapper teacherMapper;

    @Override
    public List selectDataName() {
        return teacherMapper.selectDataName();
    }

    @Override
    public List selectDataCount() {
        return teacherMapper.selectDataCount();
    }
}
