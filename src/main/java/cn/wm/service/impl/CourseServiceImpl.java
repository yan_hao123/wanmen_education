package cn.wm.service.impl;

import cn.wm.model.Course;
import cn.wm.mapper.CourseMapper;
import cn.wm.service.CourseService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-13
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements CourseService {

    @Autowired
    private CourseMapper courseMapper;

    @Override
    public IPage findAllPage(Integer pageNo,Integer pageSize,String courseName) {
        Page page = new Page(pageNo,pageSize);
        IPage iPage = courseMapper.findAllPage(page,courseName);
        return iPage;
    }

    @Override
    public List<Course> findAll() {
        return courseMapper.findAll();
    }

}
