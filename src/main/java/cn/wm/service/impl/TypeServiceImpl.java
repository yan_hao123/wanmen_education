package cn.wm.service.impl;

import cn.wm.mapper.ClasstypeMapper;
import cn.wm.model.Type;
import cn.wm.mapper.TypeMapper;
import cn.wm.service.TypeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@Service
public class TypeServiceImpl extends ServiceImpl<TypeMapper, Type> implements TypeService {

    @Autowired
    private TypeMapper typeMapper;

    @Autowired
    private ClasstypeMapper classtypeMapper;

    @Override
    public Map findSort() {
        List<Type> typeList = typeMapper.selectList(null);
        Map map = new HashMap();
        typeList.forEach(type->{
            Integer tId = type.getTId();
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("t_id",tId);
            List list = classtypeMapper.selectList(queryWrapper);
            map.put(type,list);
        });
        return map;
    }
}
