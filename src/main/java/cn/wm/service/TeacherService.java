package cn.wm.service;

import cn.wm.model.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
public interface TeacherService extends IService<Teacher> {

    List selectDataName();

    List selectDataCount();
}
