package cn.wm.service;

import cn.wm.model.Taocan;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
public interface TaocanService extends IService<Taocan> {

}
