package cn.wm.service;

import cn.wm.model.TbSysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
public interface TbSysRoleService extends IService<TbSysRole> {

}
