package cn.wm.mapper;

import cn.wm.model.ClassType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-13
 */
public interface ClasstypeMapper extends BaseMapper<ClassType> {


}
