package cn.wm.mapper;

import cn.wm.model.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-13
 */
public interface CourseMapper extends BaseMapper<Course> {

    public IPage findAllPage(Page page, @Param("courseName")String courseName);

    List<Course> findAll();
}
