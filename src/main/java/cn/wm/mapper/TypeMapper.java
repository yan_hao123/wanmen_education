package cn.wm.mapper;

import cn.wm.model.Type;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
public interface TypeMapper extends BaseMapper<Type> {

    List findAll();
}
