package cn.wm.mapper;

import cn.wm.model.TbSysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
public interface TbSysUserMapper extends BaseMapper<TbSysUser> {

}
