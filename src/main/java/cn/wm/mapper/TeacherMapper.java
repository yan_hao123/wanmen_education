package cn.wm.mapper;

import cn.wm.model.Teacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
public interface TeacherMapper extends BaseMapper<Teacher> {

    List selectDataName();

    List selectDataCount();

}
