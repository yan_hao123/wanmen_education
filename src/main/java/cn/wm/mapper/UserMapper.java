package cn.wm.mapper;

import cn.wm.model.Course;
import cn.wm.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
public interface UserMapper extends BaseMapper<User> {

    public IPage findAllPage(Page page, @Param("userName")String userName);

    List<User> findAll();

    List<User> findByName(@Param("name") String name);

    int updateUser(User user);


}
