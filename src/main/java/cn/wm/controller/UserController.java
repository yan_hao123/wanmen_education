package cn.wm.controller;


import cn.wm.model.Course;
import cn.wm.model.User;
import cn.wm.service.CourseService;
import cn.wm.service.UserService;
import cn.wm.utils.Msg;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/saveUser")
    public Msg saveUser(@RequestBody User user){
        //userService.updateUser(user);
        boolean b = userService.updateById(user);
        if(b){
            return new Msg(200,"success");
        }
        return new Msg(444,"fail");
    }

    @GetMapping("/findPage")
    @ResponseBody
    public Msg findPage(@RequestParam("pageNo")Integer pageNo,@RequestParam("pageSize")Integer pageSize){
        System.out.println("7777");
        String userName=null;
        IPage page = userService.findAllPage(pageNo, pageSize,userName);
        if(page!=null){
            System.out.println(page.getRecords());
            return new Msg(200,page);
        }
        return new Msg(444,"fail");
    }
/*
        @PostMapping("/deleteByCid")
        public Msg deleteByCid(@RequestParam("cid")Integer cid){
            boolean b = courseService.removeById(cid);
            if(b){
                return new Msg(200,"success");
            }
            return new Msg(444,"fail");
        }*/

    @GetMapping("/selectAllUser")
    public Msg selectAllUser(){
        List<User> courseList = userService.findAll();
        if(courseList!=null){
            return new Msg(200,courseList);
        } /* this.$axios.get("http://127.0.0.1:8999/course/selectAllCourse").then(res=>{
          this.courseList=res.data.data
        })*/
        return new Msg(444,"fail");
    }

    @GetMapping("/findByName")
    public Msg findByName(@RequestParam("uname")String name){
        System.out.println("3333");
        List<User> courseList = userService.findByName(name);
        if(courseList!=null){
            return new Msg(200,courseList);
        }
        return new Msg(444,"fail");
    }



}


