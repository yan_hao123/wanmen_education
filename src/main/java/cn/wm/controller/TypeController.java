package cn.wm.controller;


import cn.wm.mapper.ClasstypeMapper;
import cn.wm.model.Type;
import cn.wm.service.ClasstypeService;
import cn.wm.service.TypeService;
import cn.wm.utils.Msg;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@RestController
@RequestMapping("/type")
@CrossOrigin
public class TypeController {

    @Autowired
    private TypeService typeService;

    @Autowired
    private ClasstypeService classtypeService;

    @GetMapping("/findType")
    public Msg findType(){
        List<Type> typeList = typeService.list(null);
        if(typeList!=null){
            return new Msg(200,typeList);
        }
        return new Msg(444,"fail");
    }

    @GetMapping("/findCTypeByTid/{tid}")
    public Msg findCTypeByTid(@PathVariable("tid")Integer tid){
        QueryWrapper wrapper = new QueryWrapper<>();
        wrapper.eq("t_id",tid);
        List list = classtypeService.list(wrapper);
        if(list!=null){
            return new Msg(200,list);
        }
        return new Msg(444,"fail");
    }

}

