package cn.wm.controller;

import cn.wm.utils.Msg;
import cn.wm.utils.QiniuCloudUtil;
import cn.wm.utils.SYS;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author${闫浩} 今天，你多学一门实用的技术，
 * 明天，你就少说一句求人的话！
 * @Date 2020/9/7 15:46
 **/
@RestController
@CrossOrigin
public class UpLoadController {

    @PostMapping("/upload")
    public Msg uploadImg(@RequestParam("file") MultipartFile file){
        String path = QiniuCloudUtil.upload(file);
       if(path!=null){
           System.out.println(path);
           return new Msg(200, SYS.URL+path);
       }
        return new Msg(444,"fail");
    }

    @PostMapping("/deleteImg")
    public Msg deleteImg(@RequestParam("fileUrl")String fileUrl){
        QiniuCloudUtil.delImg(fileUrl);
        return new Msg(200,"success");
    }
}
