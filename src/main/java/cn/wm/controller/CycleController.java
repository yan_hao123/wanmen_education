package cn.wm.controller;


import cn.wm.model.Cycle;
import cn.wm.service.CycleService;
import cn.wm.utils.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@RestController
@RequestMapping("/cycle")
@CrossOrigin
public class CycleController {

    @Autowired
    private CycleService cycleService;


    @GetMapping("/findPage")
    public Msg findPage(){
        List<Cycle> list = cycleService.list(null);
        if(list!=null){
            return new Msg(200,list);
        }
        return new Msg(444,"fail");
    }


    @PostMapping("/saveCycle")
    public Msg saveCycle(@RequestBody Cycle cycle){
        boolean b = cycleService.saveOrUpdate(cycle);
        if(b){
            return new Msg(200,"success");
        }
        return  new Msg(444,"fail");
    }



    @PostMapping("/deleteById/{pid}")
    public Msg deleteById(@PathVariable("pid") Integer pid){
        boolean b = cycleService.deleteById(pid);
        if(b){
            return new Msg(200,"success");
        }
        return new Msg(444,"fail");
    }

    @GetMapping("/findOneById/{pid}")
    public Msg findOneById(@PathVariable("pid")Integer pid){
        Cycle cycle = cycleService.getById(pid);
        if(cycle!=null){
            return new Msg(200,cycle);
        }
        return new Msg(444,"fail");
    }
}

