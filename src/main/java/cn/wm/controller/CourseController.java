package cn.wm.controller;


import cn.wm.model.Course;
import cn.wm.service.CourseService;
import cn.wm.service.UserService;
import cn.wm.utils.Msg;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-13
 */
@RestController
@RequestMapping("/course")
@CrossOrigin
public class CourseController {

    @Autowired
    private CourseService courseService;

    @PostMapping("/saveCourse")
    public Msg saveCourse( Course course){
        System.out.println(course);
        boolean b = courseService.saveOrUpdate(course);
        if(b){
            return new Msg(200,"success");
        }
        return new Msg(444,"fail");
    }

    @GetMapping("/findPage")
    @ResponseBody
    public Msg findPage(@RequestParam("pageNo")Integer pageNo,@RequestParam("pageSize")Integer pageSize,@RequestParam("courseName")String courseName){
        IPage page = courseService.findAllPage(pageNo, pageSize,courseName);
        if(page!=null){
            //System.out.println(page.getTotal());
            //System.out.println(page.getRecords());
            List list = page.getRecords();
            list.forEach(e->{
                System.out.println(e);
            });
            return new Msg(200,page);
        }
        return new Msg(444,"fail");
    }

    @PostMapping("/deleteByCid")
    public Msg deleteByCid(@RequestParam("cid")Integer cid){
        boolean b = courseService.removeById(cid);
        if(b){
            return new Msg(200,"success");
        }
        return new Msg(444,"fail");
    }

    @GetMapping("/selectAllCourse")
    public Msg selectAllCourse(){
        List<Course> courseList = courseService.findAll();
        if(courseList!=null){
            return new Msg(200,courseList);
        }/*  this.$axios.get("http://127.0.0.1:8999/course/selectAllCourse").then(res=>{
          this.courseList=res.data.data
        })*/
        return new Msg(444,"fail");
    }



}

