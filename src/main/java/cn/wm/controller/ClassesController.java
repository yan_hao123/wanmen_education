package cn.wm.controller;


import cn.wm.model.Classes;
import cn.wm.service.ClassesService;
import cn.wm.service.ClasstypeService;
import cn.wm.utils.Msg;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@RestController
@RequestMapping("/classes")
@CrossOrigin
public class ClassesController {

    @Autowired
    private ClassesService classesService;

    @PostMapping("/savaClasses")
    public Msg saveClasses(@RequestBody Classes classes){
        boolean save = classesService.saveOrUpdate(classes);
        if(save){
            return new Msg(200,"success");
        }
        return new Msg(444,"fail");
    }

    @GetMapping("/findPage")
    @ResponseBody
    public Msg findPage(@RequestParam("pageNo")Integer pageNo,@RequestParam("pageSize")Integer pageSize,@RequestParam("clName")String clName){
        Page page = new Page(pageNo, pageSize);
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.like("cl_name",clName);
        wrapper.eq("status",0);
        classesService.page(page,wrapper);
        if(page!=null){
            System.out.println(page.getRecords());
            return new Msg(200,page);
        }
        return new Msg(444,"fail");
    }


    @PostMapping("/passOut")
    public Msg passOut(@RequestParam("clId")Integer clId){
        Classes classes = classesService.getById(clId);
        classes.setStatus(1);
        boolean b = classesService.updateById(classes);
        if(b){
            return new Msg(200,"success");
        }
        return new Msg(444,"fail");
    }
}

