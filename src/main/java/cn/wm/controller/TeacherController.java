package cn.wm.controller;


import cn.wm.model.Teacher;
import cn.wm.service.TeacherService;
import cn.wm.utils.Msg;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@RestController
@RequestMapping("/teacher")
@CrossOrigin
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @RequestMapping("/findAllTeacher")
    public Msg findAllTeacher(){
        List<Teacher> teacherList = teacherService.list(null);
        if(teacherList!=null){
            return new Msg(200,teacherList);
        }
        return new Msg(444,"fail");
    }

    @GetMapping("/findPage")
    @ResponseBody
    public Msg findPage(@RequestParam("pageNo")Integer pageNo,@RequestParam("pageSize")Integer pageSize,@RequestParam("thName")String thName){
        Page page = new Page(pageNo, pageSize);
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.like("th_name",thName);
        wrapper.eq("th_flag",0);
        teacherService.page(page,wrapper);
        if(page!=null){
            System.out.println(page.getRecords());
            return new Msg(200,page);
        }
        return new Msg(444,"fail");
    }


    @PostMapping("/addTeacher")
    public Msg addTeacher(@RequestBody Teacher teacher){
        boolean b = teacherService.saveOrUpdate(teacher);
        if(b){
            return new Msg(200,"success");
        }
        return new Msg(444,"fail");
    }

    @PostMapping("/passTeacher")
    public Msg passTeacher(@RequestParam("thId")Integer thId){
        boolean b = teacherService.removeById(thId);
        if(b){
            return new Msg(200,"success");
        }
        return new Msg(444,"fail");
    }

    @RequestMapping("/selectData")
    public Msg selectData(){
        List tNamelist = teacherService.selectDataName();
        List countlist = teacherService.selectDataCount();
        if(tNamelist!=null&&countlist!=null){
            return new Msg(200,tNamelist,countlist);
        }
        return new Msg(444,"fail");
    }
}

