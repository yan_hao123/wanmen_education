package cn.wm.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author${闫浩} 今天，你多学一门实用的技术，
 * 明天，你就少说一句求人的话！
 * @Date 2020/9/7 15:46
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Msg<T> {

    private Integer code;
    private String message;
    private T data;
    private List<T> list;

    public Msg(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
    public Msg(Integer code, T data) {
        this.code = code;
        this.data = data;
    }
    public Msg(Integer code, T data,List list) {
        this.code = code;
        this.data = data;
        this.list=list;
    }
}
