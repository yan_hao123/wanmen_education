package cn.wm.utils;

import java.io.*;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;

import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @Author${闫浩} 今天，你多学一门实用的技术，
 * 明天，你就少说一句求人的话！
 * @Date 2020/9/7 15:46
 **/
public class QiniuCloudUtil {
    private  static  String  accessKey = "Tuhu0y-RJhgYRdwVdZgq_PNMf5nj9y6YEPTXmJzp";
    private  static String secretKey = "h1VZPy3f55qCe7YXEICJ-_tW1XMGJsFI9i6qZXvT";
    private  static String bucket = "huazai-0108";

    public static String upload(MultipartFile multipartFile){
        //构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.region0());
//...其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);
//...生成上传凭证，然后准备上传

//默认不指定key的情况下，以文件内容的hash值作为文件名
        String key = null;
        try {
            byte[] bytes = multipartFile.getBytes();
            ByteArrayInputStream byteInputStream=new ByteArrayInputStream(bytes);
            Auth auth = Auth.create(accessKey, secretKey);
            String upToken = auth.uploadToken(bucket);
            try {
                Response response = uploadManager.put(byteInputStream,key,upToken,null, null);
                //解析上传成功的结果
                DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
                System.out.println(putRet.hash);
                return putRet.hash;
            } catch (QiniuException ex) {
                Response r = ex.response;
                System.err.println(r.toString());
                try {
                    System.err.println(r.bodyString());
                } catch (QiniuException ex2) {
                    //ignore
                }
            }
        } catch (UnsupportedEncodingException ex) {
            //ignore
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String delImg(String key){
        Configuration cfg = new Configuration(Region.region0());
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.delete(bucket, key);
        } catch (QiniuException ex) {
            //如果遇到异常，说明删除失败
            System.err.println(ex.code());
            System.err.println(ex.response.toString());
        }
        return null;
    }


}
