package cn.wm.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Date;


@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("添加数据");
        this.setFieldValByName("createTime",new Date(),metaObject);
        this.setFieldValByName("createtime",new Date(),metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
       log.info("更新数据");
       this.setFieldValByName("updatetime",new Date(),metaObject);
        this.setFieldValByName("updateTime",new Date(),metaObject);
    }

    @Bean
    @Profile({"dev","test"})// 设置 dev test 环境开启，保证我们的效率
     public PerformanceInterceptor performanceInterceptor() {
         PerformanceInterceptor performanceInterceptor = new PerformanceInterceptor();
         performanceInterceptor.setMaxTime(10000); // ms设置sql执行的最大时间，如果超过了则不 执行    
         performanceInterceptor.setFormat(true); // 是否格式化代码    
         return performanceInterceptor;
    }

}
