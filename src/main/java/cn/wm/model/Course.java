package cn.wm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Course对象", description="")
public class Course implements Serializable {
/*(cId=null, cName=vsdvdfv, cPic=http://qhsf27w6d.hd-bkt.clouddn.com/Fj3aVQ9WLg3rURP-dTjL7Sh7ct__,
 cInfo=fsdvbsb, cTime=null, cPrice=198.0, thId=1, tId=1, cDetails=dvfbvsbvsrbdfb,
cPic1=http://qhsf27w6d.hd-bkt.clouddn.com/Fj3aVQ9WLg3rURP-dTjL7Sh7ct__, cFlag=1, ctId=2)*/
    private static final long serialVersionUID = 1L;

    @TableId(value = "c_id", type = IdType.AUTO)
    private Integer cId;
    private String cName;
    private String cPic;
    private String cInfo;
    private Double cTime;
    private Double cPrice;
    private Integer thId;
    private Integer tId;
    private String cDetails;
    private String cPic1;
    private Integer cFlag;
    private Integer cFlag1;
    private Integer ctId;
    private Teacher teacher;
    private Type type;
    private ClassType classType;


}
