package cn.wm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Classtype对象", description="")
public class ClassType implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ct_id", type = IdType.AUTO)
    private Integer ctId;

    private String ctName;

    private Integer ctFlag;

    private Integer tId;


}
