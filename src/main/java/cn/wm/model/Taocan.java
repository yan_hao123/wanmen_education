package cn.wm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 闫浩
 * @since 2020-10-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Taocan对象", description="")
public class Taocan implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "tc_id", type = IdType.AUTO)
    private Integer tcId;

    private String tcName;

    private String tcInfo;

    private Integer tcPrice;

    private String tcPic;


}
