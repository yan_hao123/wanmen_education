package cn.wm.config;

/**
 * @Author${闫浩} 今天，你多学一门实用的技术，
 * 明天，你就少说一句求人的话！
 * @Date 2020/9/7 15:46
 **/

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
//开启swagger的配置
@EnableSwagger2
public class SwaggerConfig {

    //将该Docket交给了spring中的ioc
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                // swagger进行包扫描，扫描你当前的controller层路径
                .apis(RequestHandlerSelectors.basePackage("cn.wm.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("java-2004swagger")
                .description("swagger接入教程，简单好用")
                //服务条款网址
                .version("111.0")
                .build();
    }
}


