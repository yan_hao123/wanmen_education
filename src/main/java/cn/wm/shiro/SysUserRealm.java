package cn.wm.shiro;

import cn.wm.model.TbSysUser;
import cn.wm.service.TbSysUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author${闫浩} 今天，你多学一门实用的技术，
 * 明天，你就少说一句求人的话！
 * @Date 2020/9/7 15:46
 **/
@Component
public class SysUserRealm extends AuthorizingRealm {

    private TbSysUserService sysUserService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String  uname = (String) token.getPrincipal();
        QueryWrapper<TbSysUser> wrapper = new QueryWrapper();
        wrapper.eq("login_name",uname);
        List<TbSysUser> list = sysUserService.list(wrapper);
        if(list.get(0)!=null){
            String pword = list.get(0).getPassword();
            return new SimpleAuthenticationInfo(uname,pword,this.getClass().getName());
        }
        return null;
    }
}